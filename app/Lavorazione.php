<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lavorazione extends Model
{
    protected $table = 'lavorazione';
    protected $primaryKey = 'lavorazione_id';

    public $timestamps = false;
}
