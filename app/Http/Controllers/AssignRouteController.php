<?php

namespace App\Http\Controllers;

use App\Lavorazione;
use App\Letturista;
use Illuminate\Http\Request;

class AssignRouteController extends Controller
{
    public function assign($sede,$progressivo, $terminale)
    {
        $routeName=Lavorazione::select('lavorazione_nome')
            ->where('lavorazione_sede_id',$sede)
            ->where('lavorazione_progressivo',$progressivo)
            ->first()->lavorazione_nome;
        $assignTo = Letturista::select('matricola')
            ->where('letturista_numero',$terminale)
            ->where('letturista_sede_id', $sede)
            ->where('letturista_attivo','1')
            ->first()->matricola;

        $url = "https://cm02.smartelements.net:8454/SensusServer/api/server/route/".$routeName."/assign?assignTo=".$assignTo;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Basic ZXRyYXNlcnZpY2U6ZXRyYXNlcnZpY2UyMDIw'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if(strtoupper(json_decode($response)->status) == "OK"){
            $value =  "OK";
        }else{
            $value = $response;
        }

        return $value;
    }
}
