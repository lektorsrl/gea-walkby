<?php

namespace App\Http\Controllers;

use App\Lavorazione;
use App\Letture;
use Illuminate\Http\Request;

class CreateRouteController extends Controller
{
    public function create($progressivo)
    {
        $routeName = Lavorazione::where('lavorazione_progressivo',$progressivo)->value('lavorazione_nome');
        $meters = Letture::select('matricola','comune', 'utente', 'indirizzo', 'civico', 'barrato', 'ubicazione', 'codice_utente','latitudine','longitudine')->where('stato','NUL')
            ->where('progressivo',$progressivo)
            ->get();
        $i = 0;
        foreach ($meters as $meter){
            $i = $i +1;
            $dati[$i]['siteId']=$meter->anagraficaContatore->matricola_radio;
            $dati[$i]['amrChannel'] = '8';
            $dati[$i]['exMeterId'] = $meter->matricola;
            $dati[$i]['srfAddress']=$meter->anagraficaContatore->matricola_radio;
            $dati[$i]['srfKey']=$meter->anagraficaContatore->chiave;
            $dati[$i]['unit']='l';
            $dati[$i]['city']=$meter->comune;
            $dati[$i]['routeName']=$routeName;
            $dati[$i]['customerName']=$meter->utente;
            $dati[$i]['customerInfo']=$meter->codice_utente;
            $dati[$i]['street']=$meter->indirizzo;
            $dati[$i]['stN1']=$meter->civico." ".$meter->barrato;
            $dati[$i]['siteRemark']=$meter->ubicazione;
            $dati[$i]['latitudine']=$meter->latitudine;
            $dati[$i]['longitudine']=$meter->longitudine;
            $dati[$i]['sequence']=$i;
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://cm02.smartelements.net:8454/SensusServer/api/server/meters?update=true&createRoutes=true',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>json_encode(array_values($dati)),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Basic ZXRyYXNlcnZpY2U6ZXRyYXNlcnZpY2UyMDIw'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        if(strtoupper(json_decode($response)->status) == "OK"){
            $value =  "OK";
        }else{
            $value =  $response;
        }
        return $value;
    }
}
