<?php

namespace App\Http\Controllers;

use App\Lavorazione;
use App\Letture;
use Illuminate\Http\Request;

class ExportRouteController extends Controller
{
    public function export($sede,$progressivo)
    {
        $routeName=Lavorazione::select('lavorazione_nome')
            ->where('lavorazione_sede_id',$sede)
            ->where('lavorazione_progressivo',$progressivo)
            ->first()->lavorazione_nome;

        //$routeName = "Thiene4";  //SOLO PER TEST

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://cm02.smartelements.net:8454/SensusServer/api/server/route/'.$routeName.'?readOnly=true&historical=true&type=LATEST',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Basic ZXRyYXNlcnZpY2U6ZXRyYXNlcnZpY2UyMDIw',
                'Cookie: JSESSIONID=0CD85E4DABF864DE437D1C8EE72077F8'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $response_decoded = json_decode($response, true);
        $cont = 0;
        foreach($response_decoded as $record){
            $matricola_radio = ($record['siteId']);
            if(array_key_exists('latitude', $record)) {
                $latitudine = ($record['latitude']);
            }else{
                $latitudine = "";
            }
            if(array_key_exists('longitude', $record)) {
                $longitudine = ($record['longitude']);
            }else{
                $longitudine = "";
            }
            foreach($record['readouts'] as $record1){
                $matricola = $record1['meterId'];
                $lettura = intval($record1['value']);
                $lettura_data_ora = date_create($record1['date']);
                $lettura_data = date_format($lettura_data_ora, "d/m/Y");
                $lettura_ora = date_format($lettura_data_ora, "H:i:s");
                $allarmi = implode(', ',($record1['alarms']));
            }
            $update = [
                'lettura' => $lettura,
                'data_lettura' => $lettura_data,
                'ora_lettura' => $lettura_ora,
                'nota' => $allarmi,
                'stato' => 'LET'
            ];
            if($latitudine != ""){
                $update['latitudine'] = $latitudine;
            }
            if($longitudine != ""){
                $update['longitudine'] = $longitudine;
            }

            $update_value = Letture::where('progressivo', $progressivo)
                ->where('matricola', $matricola)
                ->where('stato', 'CAR')
                ->update($update);

            $cont = $cont + $update_value;
        }
        return "OK - ".$cont. " record aggiornati";
    }
}
