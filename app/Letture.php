<?php

namespace App;

use App\AnagraficaContatori;
use Illuminate\Database\Eloquent\Model;

class Letture extends Model
{
    protected $connection = 'mysql_sede';
    protected $table = 'letture';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function anagraficaContatore()
    {
        return $this->hasOne(AnagraficaContatori::class, 'matricola','matricola');
    }
}
