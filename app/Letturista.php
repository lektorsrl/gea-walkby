<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Letturista extends Model
{
    protected $table = 'letturista';
    protected $primaryKey = 'letturista_id';

    public $timestamps = false;
}
