<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnagraficaContatori extends Model
{
    protected $connection = 'mysql_sede';
    protected $table = 'anagrafica_contatori';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
